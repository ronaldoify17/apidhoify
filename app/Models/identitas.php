<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class identitas extends Model
{
    protected $table = "identitas";

    protected $fillable = [
        'nama','alamat','jabatan'
    ];
}
